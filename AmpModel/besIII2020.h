#ifndef AMPMODEL_BESIII2020_H
#define AMPMODEL_BESIII2020_H

// Package.
#include "AmpModel/ampcfg.h"
#include "AmpModel/model.h"
#include "AmpModel/parameter.h"
#include "AmpModel/models/flatte.h"
#include "AmpModel/models/relbreitwigner.h"
#include "AmpModel/modeltype.h"

// SL.
#include <iostream>
#include <map>
#include <cmath>

class BESIII2020 : public ModelType
{
public:
  // Constructor/Destructor.
  BESIII2020();
  ~BESIII2020() {};

  const double to_radians(const double& degrees) const { return degrees*( M_PI/180 ); }

  AmpCfg cfg() { return besIII2020; }
  Model* model() { return besIII2020.model(); }
private:
  AmpCfg besIII2020;
};

#endif
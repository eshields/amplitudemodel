#ifndef AMPMODEL_POLYNOMIAL_H
#define AMPMODEL_POLYNOMIAL_H

// SL.
#include <iostream>
#include <vector>
#include <complex>


class Polynomial
{
public:
  /** Default constructor. */
  Polynomial() = default;
  /** Constructor. */
  Polynomial(const int& _size) : m_degree( _size ) { m_coefficients.resize( m_degree ); };
  /** Constructor. */
  Polynomial(std::initializer_list<double> coefficients);
  /** Constructor. */
  Polynomial(const int& size, double* coefficients);
  /** Constructor. */
  Polynomial(const Polynomial& other) { m_degree = other.m_degree; m_coefficients = other.m_coefficients; }

  void    setCoefficient (const int i, const double c) { m_coefficients[i] = c; };
  void    setCoefficients(std::initializer_list<double> coefficients);
  void    setCoefficients(const int& size, double* coefficients);

  int     size()    const { return m_degree; }
  double* coefficients()  { return m_coefficients.data(); }

  double  Eval(const double& x) const;
  double  operator()(const double& x) const;
private:
  int m_degree;
  std::vector<double> m_coefficients;
};

#endif
#ifndef AMPMODEL_MODELTYPE_H
#define AMPMODEL_MODELTYPE_H

#include "AmpModel/model.h"


/** @brief ModelType class that is the abstract representation of a given model.
 * 
 * Abstract class for an amplitude model that is given from an experiment.
 * Only enforces that every object must have a \see Model object that is used
 * to calculate amplitudes of that model.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class ModelType
{
public:
  /** Constructor. */
  virtual Model* model() = 0;
};

#endif
#ifndef AMPMODEL_EVENTLIST_H
#define AMPMODEL_EVENTLIST_H

// Package.
#include "AmpModel/event.h"
#include "AmpModel/decaytype.h"
#include "AmpModel/phasespace.h"
#include "AmpModel/eventgenerator.h"
#include "AmpModel/secondaries.h"
#include "AmpModel/clock.h"
#include "AmpModel/threads.h"

// SL.
#include <vector>
#include <map>


// ROOT.
#include "TTree.h"

/** @brief EventList class used to construct a toy for a given amplitude model.
 * 
 * This class will generate events, perform an accept/reject procedure on them according to an amplitude
 * model, and give a list of accepted events. 
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class EventList
{
public:
  /** Default constructor. */
  EventList(DecayType* decay, const double& mMother, const double& m1, const double& m2, const double& m3) : 
  m_decay( decay ),
  m_generators( mMother , m1 , m2 , m3 )
  {};
  /** Constructor to to include decays with secondary contamination. */
  EventList(DecayType* decay, Secondaries* sec, const double& mMother, const double& m1, const double& m2, const double& m3) : 
  m_decay( decay ),
  m_generators( sec , mMother , m1 , m2 , m3 )
  {};
  /** Destructor. */
  ~EventList() {};

  /** Will fill a list of \param events. */
  void AcceptOrReject(int nEvents = 1000)
  {
    m_decay->prepare();
    bool gen;
    m_list.resize( nEvents );
    m_efficiency.resize( m_list.size() );
    #ifdef _OPENMP
      #pragma omp parallel for
    #endif
      for (int i = 0; i < nEvents; i++) {
        gen = m_decay->accept( &(m_generators()) );
        if ( gen ) {
          m_list[i] = m_generators().event();
          m_efficiency[i] = true;
        } else m_efficiency[i] = false;
      }
    Clock::Stop();
    Clock::Print("generate "+std::to_string(nEvents)+" events:");
  };

  /** Outputs a ROOT TTree of the generated toy. */
  TTree* tree(const std::string& name) {
    TTree* outputTree = new TTree( name.c_str(), name.c_str() );

    m_event_address = *( begin() );
    for ( auto& var : m_event_address ) outputTree->Branch( (var.first).c_str() , &(var.second) );

    for (int i = 0; i < m_list.size(); i++) {
      if ( !m_efficiency[i] ) continue;
      for ( auto& var : m_event_address ) var.second = m_list[i][var.first];
      outputTree->Fill();
    }

    return outputTree;
  };

  /** Returns beginning of event list. */
  std::vector<Event>::iterator begin()                { return m_list.begin(); }
  /** Returns end of event list. */
  std::vector<Event>::iterator end()                  { return m_list.end(); }
private:
  DecayType*           m_decay;

  std::vector< Event > m_list;
  std::vector< bool >  m_efficiency;

  Event                          m_event_address;
  Threaded<EventGenerator>       m_generators;
};

#endif
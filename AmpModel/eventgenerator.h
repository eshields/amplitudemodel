#ifndef AMPMODEL_EVENTGENERATOR_H
#define AMPMODEL_EVENTGENERATOR_H

// Package.
#include "AmpModel/event.h"
#include "AmpModel/phasespace.h"
#include "AmpModel/decaytime.h"
#include "AmpModel/efficiency.h"
#include "AmpModel/secondaries.h"
#include "AmpModel/decaytime.h"
#include "AmpModel/dalitzacceptance.h"

// SL.
#include <iostream>
#include <vector>

// ROOT.
#include "TH2.h"
#include "TF1.h"

/** @brief EventGenerator class used to generate events across a flat phase-space.
 * 
 * Class generates events across a flat phases-space. Uses \see Random and \see DecayTime
 * to generate random events. These can both be configures seperately.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class EventGenerator
{
public:
  /** Default constructor taking in decay masses. */
  EventGenerator(const double& mMother, const double& m1, const double& m2, const double& m3);
  /** Constructor from another EventGenerator class. */
  EventGenerator(const EventGenerator& other);
  /** Constructor for secondary contamination. */
  EventGenerator(Secondaries* sec, const double& mMother, const double& m1, const double& m2, const double& m3);
  /** Default destructor. */
  ~EventGenerator() {};

  /** Generates an event and returns a reference to it. */
  Event& generate();

  /** Returns the last event generated/ */
  Event event() { return m_event; }

private:
  Event             m_event;
  double            m_treco;
  double            m_ttrue;
  Secondaries*      m_secondaries     = {nullptr};
  bool              m_use_secondaries = {false};
  /*
  bool              m_secondaries = {false};
  TH2D              m_hist;
  std::vector<TH1D> m_hists;
  TF1               m_func;
  */

  const PhaseSpace* m_ps;
  const double      m_mSq12min;
  const double      m_mSq12max;
  const double      m_mSq13min;
  const double      m_mSq13max;
  const double      m_mSqSum;
  Efficiency        m_efficiency;

  std::uniform_int_distribution < int >  m_integer;
};



#endif
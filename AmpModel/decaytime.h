#ifndef AMPMODEL_DECAYTIME_H
#define AMPMODEL_DECAYTIME_H

// Package.
#include "AmpModel/random.h"
#include "AmpModel/threads.h"

// json.
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// SL.
#include <iostream>
#include <string>

// ROOT.
#include "TFile.h"
#include "TH1.h"

/** @brief DecayTime class used to return decay time.
 * 
 * This class gives the decay time of a a decay. By default it will return values 
 * according to a random distribution. But it can be configured to return values
 * from a distribution provided in a ROOT histogram. This would take into account 
 * resolution and acceptance effects seen in data.
 * 
 * @author Edward Shields
 * @date   16/11/2020
 */
class DecayTime
{
public:
  /** Set the acceptance map from a configuration file. */
  static void setAcceptance(json& cfg)
  {
    std::string file = cfg["time"]["file"].get<std::string>();
    std::string obj  = cfg["time"]["name"].get<std::string>();

    m_file = new TFile(file.c_str());
    m_file->cd();
    //TH1D* tmp;
    m_file->GetObject(obj.c_str(),m_hist);
    m_hist->SetDirectory(0);
    //tmp->SetDirectory(0);
    //m_hist = tmp;
    m_file->Close();
  }

  /** Set the acceptance map from file name and object name. */
  static void setAcceptance(std::string file, std::string obj)
  {
    m_file = new TFile(file.c_str());
    m_file->cd();
    //TH1D* tmp;
    m_file->GetObject(obj.c_str(),m_hist);
    m_hist->SetDirectory(0);
    //tmp->SetDirectory(0);
    //m_hist = tmp;
    m_file->Close();
  }

  /** Returns decay time. */
  inline static double time()
  {
    if ( m_hist != nullptr ) {
      m_time() = m_hist->GetRandom();
    } else {
      m_time() = Random::exponential();
    }
    return m_time();
  }

private:
  static TFile* m_file;
  static TH1D*  m_hist;

  static Threaded<double> m_time;
};

#endif
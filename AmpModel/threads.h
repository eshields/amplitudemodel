#ifndef AMPMODEL_THREADS_H
#define AMPMODEL_THREADS_H

// SL.
#include <iostream>
#include <vector>
#include <string>

#ifdef _OPENMP
  // OpenMP.
  #include <omp.h>

  /** @brief Thread class that can control multithreading propoerties.
  * 
  * The class can set the number of threads to be used for sections of code that 
  * are multithreaded.
  * @author Edward Shields
  * @date   16/11/2020
  */
  class Threads
  {
  public:
    /** Sets number of threads to be used. */
    static void set_num_threads(const int& nThreads)
    {
      m_threads = nThreads;
      omp_set_num_threads(m_threads);
    }
    static int m_threads;
  };


   /** @brief Threaded class that handles safe multithreading of objects.
  * 
  * When the package is compiled using the OpenMP library this class provides protection 
  * that allows other objects to be used safely in a multithreaded environment. This is 
  * particulary useful to objects that make use of its own internal memory as it avoid conflicts
  * between threads accessing and writing internal class memory.
  * 
  * It essentially works by constructing a seperate object in each thread and this all the threads
  * access and write to memory unique to its thread. The approriate thread object is returned by the
  * () operator.
  * 
  * @author Edward Shields
  * @date   16/11/2020
  */
  template<typename OBJ>
  class Threaded
  {
  public:
    /** Default constructor. */
    Threaded() = default;
    /** Constructor using underlying object. */
    Threaded(OBJ obj) 
    {
      for (int i = 0; i < Threads::m_threads; i++) m_obj.push_back( obj );
    }
    /** Constructor that allows construction in the same manner as the underlying object. */
    template<class ...IN_TYPES>
    Threaded( IN_TYPES... input )
    {
      for (int i = 0; i < Threads::m_threads; i++) m_obj.push_back( OBJ( input... ) );
    }

    // Operator.
    inline OBJ& operator()()               { return m_obj[omp_get_thread_num()]; }
    inline OBJ& operator[](const int& th)  { return m_obj[th]; }
    inline Threaded<OBJ>  operator=(const OBJ& obj) 
    { if ( omp_get_num_threads() == 1 ) {
      for (int i = 0; i < omp_get_max_threads(); i++) {
        m_obj[i] = obj;
      }
    } else {
        m_obj[omp_get_thread_num()] = obj; 
      }
      return *this;
    }

  protected:
    std::vector<OBJ> m_obj;
  };
#else
  template<typename OBJ>
  class Threaded
  {
  public:
    // Constructor/Destructor.
    Threaded() = default;
    Threaded(OBJ obj)
    {
      m_obj.push_back( obj );  
    }
    template<class ...IN_TYPES>
    Threaded( IN_TYPES... input )
    {
      m_obj.push_back( OBJ( input... ) );
    }

    // Operator.
    inline OBJ& operator()()              { return m_obj[0]; }
    inline OBJ& operator[](const int& th) { return m_obj[0]; }
  private:
    std::vector<OBJ> m_obj;
  };
#endif

#endif
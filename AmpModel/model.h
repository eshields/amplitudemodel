#ifndef AMPMODEL_MODEL_H
#define AMPMODEL_MODEL_H

// Package.
#include "AmpModel/phasespace.h"
#include "AmpModel/resonance.h"

// SL.
#include <iostream>
#include <vector>
#include <complex>
#include <map>

/** @brief Model class that represents an amplitude model constructed from a series of resonances.
 * 
 * This class holds the indiviudal resonances that make up an amplitude model and calculates the 
 * amplitude by iterating over all the resonances and summing their amplitudes multiplied by their 
 * coefficients.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class Model
{
friend class DecayType;
friend class CoherentSum;
friend class Mixing;
public:
  /** Constructor. */
  Model(const double& mMother, const double& m1, const double& m2, const double& m3)
  : _ps( PhaseSpace(mMother, m1, m2, m3) ) {};
  /** Destructor. */
  ~Model() {};

  /** Calculates amplitudes and assigns value to internal memory. */
  void                   calculate(const double& mSq12, const double& mSq13);
  /** Calculates amplitudes and assigns value to internal memory. */
  void                   calculate(const double& mSq12, const double& mSq13, const double& mSq23);

  /** Returns amplitude, \f$\mathcal{A}$\f. */
  std::complex< double > dirVal(const double& mSq12, const double& mSq13);
  /** Return the conjugate amplitude, \f$\bar{\mathcal{A}}$\f. */
  std::complex< double > cnjVal(const double& mSq12, const double& mSq13);
  /** Returns amplitude, \f$\mathcal{A}$\f. */
  std::complex< double > dirVal(const double& mSq12, const double& mSq13, const double& mSq23);
  /** Return the conjugate amplitude, \f$\bar{\mathcal{A}}$\f. */
  std::complex< double > cnjVal(const double& mSq12, const double& mSq13, const double& mSq23);

  /** Returns amplitude, \f$\mathcal{A}$\f. */
  std::complex< double > Adir  () const;
  /** Returns the conjugate ampliutde , \f$\mathcal{A}^{2}$\f. */
  std::complex< double > Abar  () const;

  /** Returns the CP-even amplitude, \f$\mathcal{A} + \bar{\mathcal{A}}/2$\f. */
  std::complex< double > A1    () const;
  /** Returns the CP-odd amplitude, \f$\mathcal{A} - \bar{\mathcal{A}}/2$\f. */
  std::complex< double > A2    () const;

  /** Returns the ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqdir() const;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqbar() const;

  /** Returns the CP-even amplitude squared, \f$\left(\mathcal{A} + \bar{\mathcal{A}}/2\right)^{2}$\f. */
  double                 ASq1  () const;
  /** Returns the CP-odd amplitude squared, \f$\left(\mathcal{A} - \bar{\mathcal{A}}/2\right)^{2}$\f. */
  double                 ASq2  () const;

  /** Returnes total amplitude squared, \f$\left(\mathcal{A} + \bar{\mathcal{A}}\right)^{2}$\f. */
  double                 ASq   () const;

  // Specific resonance.
  /** Returns amplitude, \f$\mathcal{A}$\f, of a single resonance. */
  std::complex< double > Adir  (const char* name) const;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f of a single resonance. */
  std::complex< double > Abar  (const char* name) const;

  /** Returns the ampliutde squared, \f$\mathcal{A}^{2}$\f, of a single resonance. */
  double                 ASqdir(const char* name) const;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f, of a single resonance. */
  double                 ASqbar(const char* name) const;

  /** Returnes total amplitude squared, \f$\left(\mathcal{A} + \bar{\mathcal{A}}\right)^{2}$\f, of a single resonance. */
  double                 ASq   (const char* name) const;

  // Phase space.
  double mSq12min() const { return _ps.mSq12min(); } 
  double mSq12max() const { return _ps.mSq12max(); }
  double mSq13min() const { return _ps.mSq13min(); }
  double mSq13max() const { return _ps.mSq13max(); }
  double mSq23min() const { return _ps.mSq23min(); }
  double mSq23max() const { return _ps.mSq23max(); }
  double mSq23(const double& mSq12, const double& mSq13) const { return _ps.mSqSum() - mSq12 - mSq13; } 

  // Add resonances to model.
  void add_resonance(Resonance* res);
  int resos() { return _ampDir.size(); }
  PhaseSpace ps() { return _ps; }

  // Normalize.
  void setNorm();

  Model operator+=(Resonance* res);
  Model operator=(Model model);
private:
  PhaseSpace _ps;
  std::map<const char* ,Resonance*> _ampDir;
  std::map<const char* ,Resonance*> _ampCnj;

  // Event storage.
  std::map<const char* ,std::complex<double> > _dirVal;
  std::map<const char* ,std::complex<double> > _cnjVal;

  std::complex<double> _DirVal;
  std::complex<double> _CnjVal;
};

#endif
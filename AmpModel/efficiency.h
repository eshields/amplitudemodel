#ifndef AMPMODEL_EFFICIENCY_H
#define AMPMODEL_EFFICIENCY_H

// SL.
#include <iostream>

// json.
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// ROOT.
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"

/** @brief Efficiency class used to acceptance efficiency of a given event.
 * 
 * Given an event this class will return an efficinecy of that event with a maximum of 1.
 * Three types of acceptance effects are considered.
 * - Decay time acceptance.
 * - Dalitz plot acceptance.
 * - Correlation efficiency effects between decay time and a dalitz variable.
 * The returned efficiency is the product of all efficiency effects.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class Efficiency
{
public:
  /** Default constructor. */
  Efficiency() = default;
  /** Constructor from another effiecieny object. */
  Efficiency(const Efficiency& eff);
  /** Destructor. */
  ~Efficiency() {};

  /** Set the correlation acceptance from a histogram in a TFile. */
  void setCorrelationAcceptance (std::string& file_name, std::string& obj_name);

  /** Set tthe correlation acceptance from a histogram in a TFile. */
  void setAcceptance(json& cfg);

  /** Operator providing the efficiency of an event. */
  const double operator() (const double& t, const double& mSq12, const double& mSq13, const double& mSq23);

private:
  // Correlation effects.
  TFile* m_corrF;             /** < TFile containing correlation map. */
  TH2D   m_corrH;             /** < Histogram with correlation map. */
  bool   m_corrS   = {false}; /** < Boolean of whether correlation acceptance effects are included. */
};

#endif
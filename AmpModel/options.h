#ifndef AMPMODEL_OPTIONS_H
#define AMPMODEL_OPTIONS_H

// SL.
#include <iostream>
#include <string>

/** @brief Options class used to configure various global settings.
 * 
 * This class contains a series of static options that can be accessed globally
 * and are common across all objects that use them. Shoudl be configured at the
 * beginning of executable to all subsequent objects work with the correct options.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class Options
{
private:
  static std::string _coordinates;

  static std::string _angularunits;

  static std::string _angular;

  static bool        _randompar;
public:
  /** Return coordinates system. */
  static std::string Coordinates()                             { return _coordinates; }
  /** Set coordinate system. Default is polar.*/
  static void        SetCoordinates(std::string coordinates)   { _coordinates = coordinates; }

  /** Return angular units. */
  static std::string AngularUnits()                            { return _angularunits; }
  /** Set angular units. Default is radians. */
  static void        SetAngularUnits(std::string angularunits) { _angularunits = angularunits; }

  /** Return angular formalism. */
  static std::string Angular()                                 { return _angular; }
  /** Set angular formalism. Default it zemach. */
  static void        SetAngular(std::string angular)           { _angular = angular; }

  /** Returns if parameters have a random variation. */
  static bool        ParRandom()                               { return _randompar; }
  /** Set if parameters are to be varied within their error. Default is false. */
  static void        SetParRandom(bool randompar = true)       { _randompar = randompar; }
};


#endif
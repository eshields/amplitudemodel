#ifndef AMPMODEL_DECAYTYPE_H
#define AMPMODEL_DECAYTYPE_H

// Package.
#include "AmpModel/model.h"
#include "AmpModel/threads.h"
#include "AmpModel/eventgenerator.h"
#include "AmpModel/threads.h"

/** @brief DecayType class used to provide ampltitudes.
 * 
 * Abstract class that gives the amplitudes of an amplitude model.
 * Additionally it provides methods for toy generation by preparing a model for toy generation
 * and providing a method to accept certain events according to an amplitude model.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class DecayType
{
public:
  /** Constructor. */
  DecayType(Model* model, Efficiency eff) : m_model( model ) , m_efficiency( eff ) {};
  
  /** Calculate amplitudes and assign values to internal memory. */
  void calculate(const double& mSq12, const double& mSq13)                      const { m_model->calculate(mSq12,mSq13);       }
  /** Calculate amplitudes and assign values to internal memory. */
  void calculate(const double& mSq12, const double& mSq13, const double& mSq23) const { m_model->calculate(mSq12,mSq13,mSq23); }

  // Amplitude methods
  /** Returns the amplitude, \f$\mathcal{A}$\f. */
  virtual std::complex< double > Adir  (const double& t, const double& mSq12, const double& mSq13)                      const = 0;
  /** Returns the amplitude, \f$\mathcal{A}$\f. */
  virtual std::complex< double > Adir  (const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const = 0;
  /** Return the conjugate amplitude, \f$\bar{\mathcal{A}}$\f. */
  virtual std::complex< double > Abar  (const double& t, const double& mSq12, const double& mSq13)                      const = 0;
  /** Return the conjugate amplitude, \f$\bar{\mathcal{A}}$\f. */
  virtual std::complex< double > Abar  (const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const = 0;

  /** Returns the ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  virtual double                 ASqdir(const double& t, const double& mSq12, const double& mSq13)                      const = 0;
  /** Returns the ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  virtual double                 ASqdir(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const = 0;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  virtual double                 ASqbar(const double& t, const double& mSq12, const double& mSq13)                      const = 0;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  virtual double                 ASqbar(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const = 0;

  // Phase-space methods.
  /** Returns minimum of dalitz variable mSq12. */
  double                 mSq12min() const { return m_model->mSq12min(); }
  /** Returns maximum of dalitz variable mSq12. */
  double                 mSq12max() const { return m_model->mSq12max(); }
  /** Returns minimum of dalitz variable mSq13. */
  double                 mSq13min() const { return m_model->mSq13min(); }
  /** Returns maximim of dalitz variable mSq13. */
  double                 mSq13max() const { return m_model->mSq13max(); }
  /** Returns minimum of dalitz variable mSq23. */
  double                 mSq23min() const { return m_model->mSq23min(); }
  /** Returns maximum of dalitz variable mSq23. */
  double                 mSq23max() const { return m_model->mSq23max(); }
  /** Returns mSq23 given other two dalitz variables. */
  double                 mSq23(const double& mSq12, const double& mSq13) const { return m_model->mSq23(mSq12, mSq13); }

  // Toy geneation methods.
  /** Prepare model for toy generation. */
  void prepare();
  /** Given an \see EventGenerator, accept an eventat random according to the amplitude model. */
  bool accept(EventGenerator* generator);

protected:
  Model* m_model;
  double m_maxPdf;

  Efficiency m_efficiency;
};

#endif
#ifndef AMPMODEL_BELLE2010_H
#define AMPMODEL_BELLE2010_H

// Package.
#include "AmpModel/ampcfg.h"
#include "AmpModel/model.h"
#include "AmpModel/parameter.h"
#include "AmpModel/models/flatte.h"
#include "AmpModel/models/relbreitwigner.h"
#include "AmpModel/modeltype.h"

// SL.
#include <iostream>
#include <map>

class Belle2010 : public ModelType
{
public:
  // Constructor/Destructor.
  Belle2010();
  ~Belle2010() {};

  AmpCfg* cfg() { return &belle2010; }
  Model* model() { return belle2010.model(); }
private:
  AmpCfg belle2010;
};

#endif
#ifndef AMPMODEL_AMPCFG_H
#define AMPMODEL_AMPCFG_H

// Package.
#include "AmpModel/model.h"
#include "AmpModel/resonance.h"
#include "AmpModel/phasespace.h"
#include "AmpModel/parameter.h"
#include "AmpModel/coefficient.h"
#include "AmpModel/models/relbreitwigner.h"
#include "AmpModel/models/flatte.h"

// SL.
#include <iostream>
#include <vector>

class AmpCfg
{
public:
  // Constructor/Destructor.
  AmpCfg(const double& mMother, const double& m1, const double& m2, const double& m3)
  : _model( new Model(mMother, m1, m2, m3) )
  {};
  ~AmpCfg() {};

  // Add resonances to model.
  void add(const char* name, Coeff& coeff, const int& resA, const int& resB,
           Parameter& mass, Parameter& width, const int l, Parameter& r);
  void add(const char* name, Parameter& c1, Parameter& c2, const int& resA, const int& resB,
           Parameter& mass, Parameter& width, const int l, Parameter& r);
  void add(const char* name, Coeff& coeff, const int& resA, const int& resB,
           Parameter& mass, Parameter& width, const int l, Parameter& r,
           Parameter& gam1, Parameter& gam2, Parameter& mA2, Parameter& mB2);
  void add(const char* name, Parameter& c1, Parameter& c2, const int& resA, const int& resB,
           Parameter& mass, Parameter& width, const int l, Parameter& r,
           Parameter& gam1, Parameter& gam2, Parameter& mA2, Parameter& mB2);

  void resonance(Resonance& res);
  void resonance(Resonance* res);

  void setNorm() { _model->setNorm(); }

  Model* model() { return _model; }
private:
  Model* _model;

  std::vector< Parameter* > _params;
};

#endif
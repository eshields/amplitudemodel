#ifndef AMPMODEL_COHERENTSUM_H
#define AMPMODEL_COHERENTSUM_H

// Package.
#include "AmpModel/model.h"
#include "AmpModel/decaytype.h"

/** @brief CoherentSum class used to provide ampltitudes.
 * 
 * Gives the amplitudes of a model built from the coherent sum of a series of resonaces.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class CoherentSum : public DecayType
{
public:
  /** Constructor. */
  CoherentSum(Model* model, Efficiency eff) : DecayType( model , eff ), _DirValPtr( &model->_DirVal), _CnjValPrt( &model->_CnjVal ) {};
  /** Destructor. */ 
  ~CoherentSum() {};

  // Amplitude methods.
  /** Returns the amplitude, \f$\mathcal{A}$\f. */
  std::complex< double > Adir  (const double& t) const;
  /** Return the conjugate amplitude, \f$\bar{\mathcal{A}}$\f. */
  std::complex< double > Abar  (const double& t) const;

  /** Returns the ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqdir(const double& t) const;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqbar(const double& t) const;

private:
  const std::complex<double>* _DirValPtr;
  const std::complex<double>* _CnjValPrt;
};

#endif
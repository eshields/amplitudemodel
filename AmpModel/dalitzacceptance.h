#ifndef AMPMODEL_DALITZACCEPTANCE_H
#define AMPMODEL_DALITZACCEPTANCE_H

// Package.
#include "AmpModel/threads.h"
#include "AmpModel/random.h"

// json.
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// SL/
#include <iostream>
#include <string>
#include <complex>
#include <vector>

// ROOT.
#include "TFile.h"
#include "TH2.h"

/** @brief DalitzAcceptance class used to generate events according to a dalitz accpetance.
 * 
 * This class uses a 2 dimensional histogram from ROOT that gives the acceptance oce a dalitz plot
 * and returns random points within the dalitz plot according to its acceptance. It worls on the assumption
 * that both will be called at the same time. I.e. you wont call mSq12 twice in a row, rather mSq12
 * then mSq13.
 * 
 * @author Edward Shields
 * @date   16/11/2020
 */
class DalitzAcceptance
{
public:
  /** Set the acceptance map from a configuration file. */
  static void setAcceptance(json& cfg)
  {
    std::string file = cfg["dalitz"]["file"].get<std::string>();
    std::string obj  = cfg["dalitz"]["name"].get<std::string>();

    m_file = new TFile(file.c_str());
    m_file->cd();
    TH2D* tmp;
    m_file->GetObject(obj.c_str(),tmp);
    tmp->SetDirectory(0);
    m_hist = tmp;
    m_file->Close();

    #ifdef _OPENMP
      fillList();
    #endif
  }

  /** Set the acceptance map from file name and object name. */
  static void setAcceptance(std::string file, std::string obj)
  {
    m_file = new TFile(file.c_str());
    m_file->cd();
    TH2D* tmp;
    m_file->GetObject(obj.c_str(),m_hist);
    m_hist->SetDirectory(0);
    m_hist = tmp;
    m_file->Close();

    #ifdef _OPENMP
      fillList();
    #endif
  }

  /** Set the acceotance map using the masses of particles. */
  static void setAcceptance(double mM, double m1, double m2, double m3)
  {
    double mSqSum = std::pow( m1 , 2 ) + std::pow( m2 , 2 ) + std::pow( m3 , 2 );
    m_mSq12min = std::pow( ( m1 + m2 ) , 2 );
    m_mSq12max = std::pow( ( mM - m3 ) , 2 );
    m_mSq13min = std::pow( ( m1 + m3 ) , 2 );
    m_mSq13max = std::pow( ( mM - m2 ) , 2 );

    #ifdef _OPENMP
      fillList();
    #endif
  }

  static void fillList(const int& offset = 0)
  {
    int n = 10000;
    m_mSq12_list.resize( n + offset );
    m_mSq13_list.resize( n + offset );
    for (int i = offset; i < n + offset; i++) {
      double mSq12tmp, mSq13tmp;
      if ( m_hist != nullptr)
        m_hist->GetRandom2( mSq12tmp , mSq13tmp );
      else {
        mSq12tmp = Random::flat( m_mSq12min , m_mSq12max );
        mSq13tmp = Random::flat( m_mSq13min , m_mSq13max );
      }
      m_mSq12_list[i] = mSq12tmp;
      m_mSq13_list[i] = mSq13tmp;
    }
  }
  
  /** Generate a random point in phase-space and return mSq12. */
  inline static double mSq12() 
  {
    #ifdef _OPENMP
      int i = m_counter;
      m_mSq12() = m_mSq12_list[i];
      m_mSq13() = m_mSq13_list[i];
      m_counter++;
      #pragma omp critical
        if ( m_counter++ > m_mSq12_list.size() ) fillList( m_mSq12_list.size() );
    #else
      if ( m_hist != nullptr) {
        m_hist->GetRandom2(m_mSq12(),m_mSq13());
      } else {
       m_mSq12() = Random::flat( m_mSq12min , m_mSq12max );
       m_mSq13() = Random::flat( m_mSq13min , m_mSq13max );
      }
    #endif

    return m_mSq12();
  }

  /** Return mSq13. */
  inline static double mSq13()
  {
    return m_mSq13();
  }

private:
  static TFile* m_file;
  static TH2D*                   m_hist;
  static Threaded<double>        m_mSq12;
  static Threaded<double>        m_mSq13;

  // Phase space parameters.
  static double m_mSq12min;
  static double m_mSq12max;
  static double m_mSq13min;
  static double m_mSq13max;

  static std::vector<double> m_mSq12_list;
  static std::vector<double> m_mSq13_list;
  static int m_counter;
};

#endif
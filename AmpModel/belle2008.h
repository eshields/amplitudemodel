#ifndef AMPMODEL_BELLE2008_H
#define AMPMODEL_BELLE2008_H

// Package.
#include "AmpModel/ampcfg.h"
#include "AmpModel/model.h"
#include "AmpModel/parameter.h"
#include "AmpModel/models/flatte.h"
#include "AmpModel/models/relbreitwigner.h"
#include "AmpModel/options.h"
#include "AmpModel/modeltype.h"


// SL.
#include <iostream>
#include <map>
#include <cmath>

class Belle2008 : public ModelType
{
public:
  // Constructor/Destructor.
  Belle2008();
  ~Belle2008() {};

  const double to_radians(const double& degrees) const { return degrees*( M_PI/180 ); }

  AmpCfg* cfg()  { return &belle2008; }
  Model* model() { return belle2008.model(); }
private:
  AmpCfg belle2008;
};

#endif
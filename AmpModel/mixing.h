#ifndef AMPMODEL_MIXING_H
#define AMPMODEL_MIXING_H

// Package.
#include "AmpModel/model.h"
#include "AmpModel/decaytype.h"

// SL.
#include <iostream>
#include <complex> 

/** @brief Mixing class used to provide ampltitudes, including mixing and CP-violation.
 * 
 * Gives the amplitudes of a model constructed from a series of resonances. It can include mixing 
 * and CP-violation effects by calculating the ampllitude as
 * \f[
 *    \mathcal{A}\left( t \right) = g_{+}\left( t \right)\mathcal{A} 
 *                                  + \frac{q}{p}g_{-}\left( t \right)\bar{\mathcal{A}}
 * \f]
 * and 
 * \f[
 *    \bar{\mathcal{A}}\left( t \right) = \frac{p}{q}g_{-}\left( t \right)\mathcal{A} 
 *                                         + g_{+}\left( t \right)\mathcal{A}
 * \f]
 * where \f$g_{+}\left( t \right) = e^{-iMt}e^{i\Gamma t/2}\cos \left( -i \left( x + iy \right) \Gamma t/2 \right)$\f
 * and \f$g_{-}\left( t \right) = e^{-iMt}e^{i\Gamma t/2}\sin \left( -i \left( x + iy \right) \Gamma t/2 \right)$\f.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class Mixing : public DecayType
{
public:
  /** Constructor. */
  Mixing(Model* model, Efficiency eff, const double& x = 0.004, const double& y = 0.006, const double& p = 1, const double& q = 1) : DecayType( model , eff ), _DirValPtr( &model->_DirVal), _CnjValPrt( &model->_CnjVal ), _x( x ), _y( y ), _z( x , y ), _p( p ), _q( q ) {};
  /** Destructor. */
  ~Mixing() {};

  /** Returns \f$g_{+}\left( t \right) = e^{-iMt}e^{i\Gamma t/2}\cos \left( -i \left( x + iy \right) \Gamma t/2 \right)$\f. */
  std::complex< double > gp    (const double& t) const { return std::cosh( _z * ( t/2 ) ); }
  /** \f$g_{-}\left( t \right) = e^{-iMt}e^{i\Gamma t/2}\sin \left( -i \left( x + iy \right) \Gamma t/2 \right)$\f. */
  std::complex< double > gm    (const double& t) const { return -std::sinh( _z * ( t/2 ) ); }

  /** Returns the amplitude, \f$\mathcal{A}$\f. */
  std::complex< double > Adir  (const double& t, const double& mSq12, const double& mSq13) const;
  /** Return the conjugate amplitude, \f$\bar{\mathcal{A}}$\f. */
  std::complex< double > Abar  (const double& t, const double& mSq12, const double& mSq13) const;
  /** Returns the amplitude, \f$\mathcal{A}$\f. */
  std::complex< double > Adir  (const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const;
  /** Return the conjugate amplitude, \f$\bar{\mathcal{A}}$\f. */
  std::complex< double > Abar  (const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const;

  /** Returns the ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqdir(const double& t, const double& mSq12, const double& mSq13) const;
  /** Returns the ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqbar(const double& t, const double& mSq12, const double& mSq13) const;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqdir(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const;
  /** Returns the conjugate ampliutde squared, \f$\mathcal{A}^{2}$\f. */
  double                 ASqbar(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const;

private:
  const std::complex<double>* _DirValPtr;
  const std::complex<double>* _CnjValPrt;

  // Mixing parameters.
  const double _x;
  const double _y;
  const std::complex<double> _z;
  // CP parameters.
  const double _p;
  const double _q;
};

#endif
#ifndef D02KSKK_DALITZPLOT_H
#define D02KSKK_DALITZPLOT_H

// Package.
#include "AmpModel/phasespace.h"

// SL.
#include <iostream>
#include <string>
#include <vector>

// ROOT.
#include "TCanvas.h"
#include "TH2.h"
#include "TGraph.h"
#include "TStyle.h"

/** @brief DalitzPlot class used to plots and projections of a Dalitz Plot.
 * 
 * Given the masses of the of the particles invloved in the decay, this class
 * provides methods for plotting an amplitude models dalitz plot and projections
 * of it. Further methods can be used to configure the plots.
 * 
 * @author Edward Shields
 * @date   05/11/2020
 */
class DalitzPlot
{
public:
  /** Constructor. */
  DalitzPlot(const double& mMother, const double& m1, const double& m2, const double& m3)
  : _ps( new PhaseSpace(mMother, m1, m2, m3) ) {};
  /** Destructor. */
  ~DalitzPlot() {};

  /** Returns canvas with Dalitz plot. */
  TCanvas* plot(TH2* his);
  /** Returns projection of Dalitz plot. */
  TCanvas* plotProjection(TH2* his, std::string var = "x");
  /** Returns projections of multiple dalitz plots. Takes vector of histograms as input. */
  TCanvas* plotProjection(std::vector<TH2*> hiss, std::string var = "x");

  /** Set unit of variables. */
  void setUnit(std::string unit) { _unit = unit; }
  /** Set X-axis title. */
  void setXtitle(std::string xaxis) { _xaxis = xaxis; }
  /** Set Y-axis title. */
  void setYtitle(std::string yaxis) { _yaxis = yaxis; }

private:
  void decorate(TH2* his);
  void setBoundaryOptions(TGraph* boundary);
  TGraph* setBoundary(const int& boundary_points = 1000);

  std::string _unit = {std::string("MeV/#it{c}^{2}")};
  std::string _xaxis = {std::string("m^{2}(K_{S}^{0}K^{+})")};
  std::string _yaxis = {std::string("m^{2}(K_{S}^{0}K^{-})")};

  PhaseSpace* _ps;
};

#endif
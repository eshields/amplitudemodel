#ifndef AMPMODEL_SECONDARIES_H
#define AMPMODEL_SECONDARIES_H

// Package.
#include "AmpModel/polynomial.h"
#include "AmpModel/threads.h"

// SL.
#include <iostream>
#include <vector>
#include <string>

// json.
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// ROOT.
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"

class Secondaries
{
public:
  /** Default constructor. */
  Secondaries() = default;
  /** Constructor from another secondaries object. */
  Secondaries(const Secondaries& sec);
  /** Constructor from config file. */
  Secondaries(const json& cfg);
  /** Construcotr with file paths. */
  Secondaries(const std::string& hist_file, const std::string& hist_obj, const std::string& func_file, const std::string& func_obj);
  /** Destructor. */
  ~Secondaries() {};

  const double true_time  (const double& t_reco);
  const double fraction   (const double& t_reco);
  const double operator() (const double& t_reco);
private:
  void m_construct_hist_vector();
  // Histogram mappings.
  TFile*             m_histF;
  TH2D*              m_hist;
  std::vector<TH1D*> m_hists;

  // Secondaries fraction.
  TFile*             m_funcF;
  Polynomial         m_func;

};

#endif
#!/bin/bash

# Set up variables.
AMP_ROOT=${PWD}
TOY_ROOT=/eos/lhcb/user/e/eshields/D02KSKK/d02kskk-toys/

# Generate scripts.
python3 macros/submitToClusterWithGanga.py --script ToyGenerator --sample Generator --nevents 1000000 --queue tomorrow
for SAMPLE in Prompt LTUNB SL 
do 
    for KSTYPE in LL DD
    do
        python3 macros/submitToClusterWithGanga.py --script ToyGenerator --sample ${SAMPLE} --kstype ${KSTYPE} --nevents 1000000 --queue tomorrow
        if [[ ! -d "${TOY_ROOT}/${SAMPLE}_${KSTYPE}" ]]
        then
            mkdir ${TOY_ROOT}/${SAMPLE}_${KSTYPE}
        fi
        python3 macros/submitToClusterWithGanga.py --script ToyGenerator --sample ${SAMPLE} --kstype ${KSTYPE} --nevents 1000000 --correlation --queue tomorrow
        if [[ ! -d "${TOY_ROOT}/${SAMPLE}_${KSTYPE}_corr" ]]
        then
            mkdir ${TOY_ROOT}/${SAMPLE}_${KSTYPE}_corr
        fi
    done
done

# Move to temporary directory where ganga files live.
cd ${HOME}/tmp

# Submit ganga jobs.
#ganga ToyGeneratorbelle2010Generator.ganga.py
#for SAMPLE in Prompt LTUNB SL 
#do
#    do
#    for KSTYPE in LL DD
#        ganga ToyGeneratorbelle2010${SAMPLE}${KSTYPE}.ganga.py
#        ganga ToyGeneratorbelle2010${SAMPLE}${KSTYPE}corr.ganga.py
#    done
#done

cd ${AMP_ROOT}

'''
This script submits a production to the Condor cluster at CERN using Ganga.
It creates the scripts to be submitted to each node of the cluster and arranges them in Ganga subjobs.
'''
import os, glob
# shortcut for environment variables
AHROOT =os.environ['ANALYSISHELPERSROOT']

from AnalysisHelpers.CondorUtils import validCondorQueues

# option parser
import argparse
parser = argparse.ArgumentParser(description='Options for submitToClusterWithGanga')
parser.add_argument('-s','--script',  default='CalculateFraction',type=str, help='set the script (default: %default)')
parser.add_argument('-m','--model',   default='belle2010',        type=str, choices=['belle2010','belle2008','besIII2020'])
parser.add_argument('--sample',       default='Generator',        type=str, choices=['Generator','Prompt','LTUNB','SL'])
parser.add_argument('--kstype',       default='LL',               type=str, choices=['LL','DD'])
parser.add_argument('--nevents',      default=1000000,            type=int, help='set the number of events to generate (default: %default)')
parser.add_argument('--secondaries',  default=False,              action='store_true')
parser.add_argument('--correlation',  default=False,              action='store_true')
parser.add_argument('-n','--subjobs', default=10,                 type=int, help='set the number of subjobs (default: %default)')
parser.add_argument('-q','--queue',   default='workday',          type=str, help='set the Condor queue (default: %default)')
parser.add_argument('--cpus',         default=1,                  type=int, help='set the number of CPUs to request (default: %default)')
args = parser.parse_args()


def CreateGangaScript(name,exePaths):
    # create temporary file
    f = open(os.environ['TMP']+'/'+name+'.ganga.py','w')
    # load list of subjobs executables
    f.write('exePaths = [\n')
    for exep in exePaths:
        f.write('\"%s\",\n' % exep)
    f.write(']\n\n')
    # prepare job
    f.write('j = Job(name="%s")\n'%name)
    f.write('j.do_auto_resubmit = \'False\'\n' +
            'j.splitter = GenericSplitter()\n' +
            'j.splitter.attribute = \'application.args\'\n' +
            'j.splitter.values = exePaths\n' +
            'j.application = Executable()\n' +
            'j.application.exe = File(\'%s/scripts/runGangaSubJob.sh\')\n\n' % os.environ['ANALYSISHELPERSROOT'])
    # Condor options
    #f.write('j.backend = Local()\n')
    f.write('j.backend = Condor()\n'+
            'j.backend.getenv = \'False\'\n'+
            'j.backend.universe = \'vanilla\'\n'+
            'j.backend.cdf_options = {\n'+
	           '\'environment\'           : \'HOME=%s\',\n' % os.environ['HOME'] +
	           '\'transfer_output_files\' : \'""\',\n' +
	           '\'+JobFlavour\'           : \'\"%s\"\',\n' % args.queue +
	           '\'RequestCpus\'           : \'%i\',\n' % args.cpus +
               '}\n\n' )
    # submit
    f.write('j.submit()\n')
    f.close()
    return

def CreateExecutable(dirName,N):
    if not os.path.exists(dirName): os.makedirs(dirName)
    f = open(dirName+'/run.sh','w')
    f.write('#!/bin/sh\n')
    f.write('export ANALYSISHELPERSROOT=%s\n' % AHROOT)
    f.write('export PYTHONPATH=${PYTHONPATH}:%s/python\n' % AHROOT)
    f.write('export AMPMODELROOT=%s\n'%(os.getcwd()))
    f.write('cd $AMPMODELROOT\n')
    f.write('source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-centos7-gcc8-opt\n') 
    if ( args.script == 'CalculateFraction' ): f.write('for i in $(seq %i %i); do ./build/CalculateFraction %s $i; done\n'%((N*100),((N+1)*100),args.model))
    elif ( args.script == 'ToyGenerator' ): 
        f.write('for i in $(seq {start} {end}); do ./build/ToyGenerator --model {model} --nevents {nevents} --sample {sample} --kstype {kstype} --seed $i'.format(start=(N*10),end=((N+1)*10),model=args.model,nevents=args.nevents,sample=args.sample,kstype=args.kstype))
        if ( args.secondaries ): f.write(' --secondaries true')
        if ( args.correlation ): f.write(' --correlation true; done\n')
        else: f.write('; done\n')
    f.write('cd -\n')
    f.close()
    return dirName+'/run.sh'

def CreateExecutables(name=None):
    exedir = os.environ['TMP']+'/'+args.script
    if name!=None: exedir +='/'+name
    if not os.path.exists(exedir): os.makedirs(exedir)
    exelist = []
    for i in range(0,args.subjobs):
        exelist += [CreateExecutable(exedir+'/%d' %len(exelist),i)]
    return exelist

def main():
    # Check TMP enviroment variable is set and directory exists
    if not 'TMP' in os.environ.keys():
        os.environ['TMP'] = os.environ['HOME']+'/tmp'
    if not os.path.exists(os.environ['TMP']): os.makedirs(os.environ['TMP'])
    # create executables
    exePaths = CreateExecutables(args.model)
    # create option file
    scriptname = args.script+args.model
    if ( args.script == 'ToyGenerator' ):
        if ( args.sample == 'Generator' ): scriptname += args.sample
        else: scriptname+= args.sample + args.kstype
        if ( args.secondaries ): scriptname += 'secondaries'
        if ( args.correlation ): scriptname += 'corr'
    CreateGangaScript(scriptname,exePaths)
    # submit
    print('Since ganga cannot be called in python3 environment, open a new shell, go to '+os.environ['TMP']+' and call:')
    print('ganga '+scriptname+'.ganga.py')
    return

#-------------------------------
if __name__ == "__main__":
    main()
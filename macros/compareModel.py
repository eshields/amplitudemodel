import ROOT as r 

from AnalysisHelpers.ROOTUtils import LoadLHCbStyle
LoadLHCbStyle()

r.gSystem.Load('build/libAmpModel')

nbins = 1000

def getData():
    ch = r.TChain('d0kshh')
    ch.Add('/eos/lhcb/user/k/kafische/PhD/Toys/all/2218435.0/toy_13000.root')
    return ch

def fillModel(Model):
    model = r.TH2F('model','model',nbins,Model.mSq12min(),Model.mSq12max(),nbins,Model.mSq13min(),Model.mSq13max())

    for i in range(model.GetNbinsX()):
        for j in range(model.GetNbinsY()):
            mSq12 = model.GetXaxis().GetBinCenter(i)
            mSq13 = model.GetYaxis().GetBinCenter(j)
            model.SetBinContent( i , j , Model.ASq(mSq12,mSq13))

    return model


def fillData(ch,Model):
    data = r.TH2F('data','data',nbins,Model.mSq12min(),Model.mSq12max(),nbins,Model.mSq13min(),Model.mSq13max())

    for ev in ch:
        data.Fill( ch.mSq12 , ch.mSq23 )

    return data

def main():
    # Load model.
    Model = r.Belle2010().model()
    # Load data.
    ch = getData()

    # Fill histograms.
    model = fillModel(Model)
    data = fillData(ch,Model)

    # Normalise.
    model.Scale( 1/model.GetEntries() )
    data.Scale( model.GetEntries()/data.GetEntries() )

    DP = r.DalitzPlot(1.8645,0.49767,0.49368,0.49368)

    modelx = model.ProjectionX()
    datax = data.ProjectionX()

    model.GetXaxis().SetTitle('m^{2}(K_{S}^{0}K^{+}) [GeV/#it{c}^{2}]')
    model.GetYaxis().SetTitle('Normalised amplitude')

    datax.SetLineColor(2)

    canv = r.TCanvas('canv','canv',650,600)
    canv.cd()
    modelx.Draw('HIST')
    #datax.Draw('HIST SAME')
    canv.Update()
    import pdb; pdb.set_trace()
    canv.Print('figs/comparison.png')



#-------------------------
if __name__ == '__main__':
    main()
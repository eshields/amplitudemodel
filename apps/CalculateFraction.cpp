#include "AmpModel/resonance.h"
#include "AmpModel/models/relbreitwigner.h"
#include "AmpModel/phasespace.h"
#include "AmpModel/model.h"
#include "AmpModel/belle2010.h"
#include "AmpModel/belle2008.h"
#include "AmpModel/besIII2020.h"
#include "AmpModel/dalitzplot.h"
#include "AmpModel/options.h"

// SL.
#include <iostream>
#include <string>
#include <fstream>

// cxxopts
#include "cxxopts.hpp"

// ROOT.
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TLegend.h"


int OnOff( const double& mSq23 ) {
  if      (   std::pow( 1.015 , 2 ) < mSq23 && mSq23 < std::pow( 1.025 , 2 ) ) return 0;
  else if ( ( std::pow( 0.900 , 2 ) < mSq23 && mSq23 < std::pow( 1.010 , 2 ) ) ||
            ( std::pow( 1.033 , 2 ) < mSq23 && mSq23 < std::pow( 1.100 , 2 ) ) ) return 1;
  return -1;
}

int main(int argc, char *argv[]) {
  // Options parser.
  cxxopts::Options options("Toy generator", "Options to generate toys.");
  options.add_options()
    ("m,model", "Model", cxxopts::value<std::string>()->default_value("belle2010"))
    ("s,seed", "Random seed", cxxopts::value<unsigned>()->default_value("0"))
  ;

  auto result = options.parse(argc, argv);

  std::string model_name = result["model"].as<std::string>();

  ModelType* model_type;
  if      ( model_name == "belle2010"  ) model_type = new Belle2010();
  else if ( model_name == "belle2008"  ) model_type = new Belle2008();
  else if ( model_name == "besIII2020" ) model_type = new BESIII2020();

  Model* model = model_type->model();

  unsigned seed = result["seed"].as<unsigned>();
  if ( seed ) {
    std::cout << "Setting seed to " << seed << std::endl;
    Options::SetParRandom();
    Random::setSeed(seed);
  }

  // Amplitudes [ ON : 0, OFF : 1 ][ A1 : 0, A2 : 1 ]
  double A[2][2];
  double step12 = ( model->mSq12max() - model->mSq12min() )/1000.;
  double step13 = ( model->mSq13max() - model->mSq13min() )/1000.;

  for (double mSq12 = model->mSq12min(); mSq12 < model->mSq12max(); mSq12 += step12 ) {
    for (double mSq13 = model->mSq13min(); mSq13 < model->mSq13max(); mSq13 += step13 ) {
      double mSq23 = model->mSq23( mSq12 , mSq13 );
      int reg = OnOff( mSq23 );
      if ( reg < 0 ) continue;
      model->calculate( mSq12 , mSq13 );
      A[reg][0] += model->ASq1();
      A[reg][1] += model->ASq2();
    }
  }

  double fON  = A[0][1] / ( A[0][0] + A[0][1] );
  double fOFF = A[1][1] / ( A[1][0] + A[1][1] );
  double f    = fON - fOFF;

  std::ofstream output;
  output.open (("/afs/cern.ch/user/e/eshields/cernbox/Analysis/branches/fitting/d02kskk-ycp/D02KSKKAnalysis/res/modelfractions/belle2010/fractions_"+std::to_string(seed)+".txt").c_str());
  output << fON  << "\n";
  output << fOFF << "\n";
  output << f    << "\n";
  output.close();

  std::cout << "fON        = " << fON  << std::endl;
  std::cout << "fOFF       = " << fOFF << std::endl;
  std::cout << "fON - fOFF = " << f    << std::endl;

  return 0;
}
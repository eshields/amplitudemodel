// Package.
#include "AmpModel/resonance.h"
#include "AmpModel/models/relbreitwigner.h"
#include "AmpModel/phasespace.h"
#include "AmpModel/model.h"
#include "AmpModel/belle2010.h"
#include "AmpModel/belle2008.h"
#include "AmpModel/besIII2020.h"
#include "AmpModel/dalitzplot.h"

// SL.
#include <iostream>
#include <string>

// cxxopts.
#include "cxxopts.hpp"

// ROOT.
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TLegend.h"
#include "TChain.h"
#include "TLeaf.h"

int OnOff( const double& mSq23 ) {
  if      (   std::pow( 1.015 , 2 ) < mSq23 && mSq23 < std::pow( 1.025 , 2 ) ) return 0;
  else if ( ( std::pow( 0.900 , 2 ) < mSq23 && mSq23 < std::pow( 1.010 , 2 ) ) ||
            ( std::pow( 1.033 , 2 ) < mSq23 && mSq23 < std::pow( 1.100 , 2 ) ) ) return 1;
  return -1;
}

void fillToy(TH2D* toy) {
  TChain* ch = new TChain("d0kshh");
  ch->Add("/eos/lhcb/user/e/eshields/D02KSKK/toys/Generator/2218435.1/toy_13001.root");

  for (int i = 0; i < ch->GetEntries(); i++) {
    ch->GetEntry( i );
    toy->Fill( ch->GetLeaf("mSq12")->GetValue() , ch->GetLeaf("mSq13")->GetValue() );
  }
}

int main(int argc, char *argv[]) {
  // Set LHCb style.
  gROOT->ProcessLine(".x src/lhcbstyle.C");

  // Options parser.
  cxxopts::Options options("Toy generator", "Options to generate toys.");
  options.add_options()
    ("m,model", "Model", cxxopts::value<std::string>()->default_value("belle2010"))
  ;

  auto result = options.parse(argc, argv);

  std::string model_name = result["model"].as<std::string>();


  ModelType* model_type;
  if      ( model_name == "belle2010"  ) model_type = new Belle2010();
  else if ( model_name == "belle2008"  ) model_type = new Belle2008();
  else if ( model_name == "besIII2020" ) model_type = new BESIII2020();

  Model* model = model_type->model();
  
  // Declare histograms.
  int nbins = 1000;
  TH2D* toy = new TH2D("toy","toy",nbins,model->mSq12min(),model->mSq12max(),nbins,model->mSq13min(),model->mSq13max());
  TH2D* full = new TH2D("full","full",nbins,model->mSq12min(),model->mSq12max(),nbins,model->mSq13min(),model->mSq13max());
  TH2D* dir = new TH2D("dir","dir",nbins,model->mSq12min(),model->mSq12max(),nbins,model->mSq13min(),model->mSq13max());
  TH2D* cnj = new TH2D("cnj","cnj",nbins,model->mSq12min(),model->mSq12max(),nbins,model->mSq13min(),model->mSq13max());
  TH2D* a1 = new TH2D("a1","a1",nbins,model->mSq12min(),model->mSq12max(),nbins,model->mSq13min(),model->mSq13max());
  TH2D* a2 = new TH2D("a2","a2",nbins,model->mSq12min(),model->mSq12max(),nbins,model->mSq13min(),model->mSq13max());

  for (int i = 1; i < full->GetNbinsX(); i++ ) {
    for (int j = 1; j < full->GetNbinsY(); j++) {
      double mSq12 = full->GetXaxis()->GetBinCenter(i);
      double mSq13 = full->GetYaxis()->GetBinCenter(j);
      double mSq23 = model->mSq23( mSq12 , mSq23 );
      model->calculate( mSq12 , mSq13 );
      full->SetBinContent( i , j , model->ASq() );
      dir->SetBinContent ( i , j , model->ASqdir() );
      cnj->SetBinContent ( i , j , model->ASqbar() );
      a1->SetBinContent  ( i , j , model->ASq1() )  ;
      a2->SetBinContent  ( i , j , model->ASq2()   );
    }
  }
  fillToy( toy );

  DalitzPlot* DP = new DalitzPlot(1.8645,0.49767,0.49368,0.49368);
  TCanvas* canv;
  canv = DP->plot( full );
  canv->Print("figs/dalitzplot.png");

  a1->SetLineColor(4);
  a2->SetLineColor(2);
  toy->SetLineColor(3);
  toy->Scale( full->Integral()/toy->Integral() );

  std::vector<TH2*> amps;
  amps.push_back(full);
  amps.push_back(a1);
  amps.push_back(a2);
  amps.push_back(toy);

  TLegend* leg = new TLegend(0.2,0.7,0.4,0.9);
  leg->AddEntry(full,"|A|^{2}","l");
  leg->AddEntry(a1,"|A_{1}|^{2}","l");
  leg->AddEntry(a2,"|A_{2}|^{2}","l");
  leg->AddEntry(toy,"Data","l");

  canv = DP->plotProjection( amps , std::string("x") );
  leg->Draw();
  canv->Print(("figs/dalitzplot_"+model_name+"_projx.png").c_str());
  canv = DP->plotProjection( amps , std::string("y") );
  leg->Draw();
  canv->Print(("figs/dalitzplot_"+model_name+"_projy.png").c_str());

  canv = DP->plotProjection( dir , std::string("x") );
  canv->Print(("figs/dalitzplot_"+model_name+"_dir_projx.png").c_str());
  canv = DP->plotProjection( dir , std::string("y") );
  canv->Print(("figs/dalitzplot_"+model_name+"_dir_projy.png").c_str());

  canv = DP->plotProjection( cnj , std::string("x") );
  canv->Print(("figs/dalitzplot_"+model_name+"_cnj_projx.png").c_str());
  canv = DP->plotProjection( cnj , std::string("y") );
  canv->Print(("figs/dalitzplot_"+model_name+"_cnj_projy.png").c_str());
  

 return 0;
}

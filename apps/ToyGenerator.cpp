// Package.
#include "AmpModel/event.h"
#include "AmpModel/eventlist.h"
#include "AmpModel/mixing.h"
#include "AmpModel/coherentsum.h"
#include "AmpModel/belle2010.h"
#include "AmpModel/belle2008.h"
#include "AmpModel/besIII2020.h"
#include "AmpModel/decaytype.h"
#include "AmpModel/modeltype.h"
#include "AmpModel/efficiency.h"
#include "AmpModel/secondaries.h"
#include "AmpModel/dalitzacceptance.h"
#include "AmpModel/threads.h"

// cxxopts.
#include "cxxopts.hpp"

// json.
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// ROOT.
#include "TFile.h"
#include "TTree.h"
#include "TRandom3.h"
#include "TH2.h"
#include "TF1.h"

// SL.
#include <iostream>
#include <fstream>
#include <string>

void setSeed(const int& seed) {
  Random::setSeed( seed );
  gRandom->SetSeed( seed );
}


int main(int argc, char *argv[]) {
  // Options parser.
  cxxopts::Options options("Toy generator", "Options to generate toys.");
  options.add_options()
    ("s,sample", "Sample", cxxopts::value<std::string>()->default_value("Generator"))
    ("k,kstype", "KS type", cxxopts::value<std::string>()->default_value("LL"))
    ("m,model", "Model", cxxopts::value<std::string>()->default_value("belle2010"))
    ("n,nevents", "Number of events", cxxopts::value<int>()->default_value("1000"))
    ("r,seed", "Random seed", cxxopts::value<int>()->default_value("0"))
    ("x,secondaries", "Secondary contamination", cxxopts::value<bool>()->default_value("false"))
    ("t,threads", "Number of threads", cxxopts::value<int>()->default_value("10"))
    ("c,correlation", "Correlation acceptance", cxxopts::value<bool>()->default_value("false"))
  ;

  auto result = options.parse(argc, argv);
  
  // Set seed.
  int seed = result["seed"].as<int>();
  setSeed( seed );

  #ifdef _OPENMP
    int nThreads = result["threads"].as<int>();
    Threads::set_num_threads( nThreads );
  #endif

  // Choose model.
  ModelType* cfg;
  if      ( result["model"].as<std::string>() == "belle2010"  ) cfg = new Belle2010();
  else if ( result["model"].as<std::string>() == "belle2008"  ) cfg = new Belle2008();
  else if ( result["model"].as<std::string>() == "besiii2020" ) cfg = new BESIII2020();

  int nEv = result["nevents"].as<int>();

  // Get data sample from input
  std::string sample = result["sample"].as<std::string>();
  std::string kstype = result["kstype"].as<std::string>();

  // Configure efficiency.
  std::ifstream i("cfg/kskk.json");
  json j;
  i >> j;
  Efficiency eff;
  bool corr = result["correlation"].as<bool>();
  if ( sample != "Generator" ) {
    if ( corr ) eff.setAcceptance( j );
    DecayTime::setAcceptance( j );
    DalitzAcceptance::setAcceptance( j );
  } else {
    DalitzAcceptance::setAcceptance( 1.8645, 0.49767, 0.49368, 0.49368 );
  }
  // Setup.
  Model* model = cfg->model();
  Mixing* amp = new Mixing(model,eff);


  // Configure secondaries.
  bool secondaries = result["secondaries"].as<bool>();
  Secondaries* sec = nullptr;
  TH2D* shist = nullptr;
  TF1*  sfunc = nullptr;
  if ( secondaries ) {
    sec = new Secondaries( j );
  }

  EventList list(amp,sec,1.8645, 0.49767, 0.49368, 0.49368);


  // Generate events.
  //list.fillList( nEv*1000 );
  list.AcceptOrReject( nEv );
  // Fill and write tree.
  std::string ouput_directory = "/eos/lhcb/user/e/eshields/D02KSKK/d02kskk-toys/";
  if ( sample == "Generator" ) ouput_directory += "Generator";
  else ouput_directory += sample+"_"+kstype;
  if ( corr ) ouput_directory += "_corr";
  ouput_directory += "/";
  std::string toy_name = ouput_directory+"toy_"+std::to_string(seed)+".root";
  TFile* file = new TFile(toy_name.c_str(),"RECREATE");
  file->cd();
  TTree* tree = list.tree("d0kshh");
  tree->Print();
  tree->Write();
  file->Close();
  std::cout << "Toy saved to: " << toy_name << std::endl;
  return 0;
}
#ifndef DICT_AMPMODELDICT_H
#define DICT_AMPMODELDICT_H 1

// Include files
#include "D02KSKK/DoNothing.h"
#include "AmpModel/ampcfg.h"
#include "AmpModel/belle2008.h"
#include "AmpModel/belle2010.h"
#include "AmpModel/besIII2020.h"
#include "AmpModel/coefficient.h"
#include "AmpModel/dalitzplot.h"
#include "AmpModel/model.h"
#include "AmpModel/parameter.h"
#include "AmpModel/phasespace.h"
#include "AmpModel/random.h"
#include "AmpModel/resonance.h"
#include "AmpModel/models/relbreitwigner.h"
#include "AmpModel/models/flatte.h"
#endif // DICT_D0LIFETIMEDICT_H

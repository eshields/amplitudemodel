#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class AmpCfg+;
#pragma link C++ class Belle2008+;
#pragma link C++ class Belle2010+;
#pragma link C++ class BesIII2020+;
#pragma link C++ class Coeff+;
#pragma link C++ class DalitzPlot+;
#pragma link C++ class Model+;
#pragma link C++ class Parameter+;
#pragma link C++ class PhaseSpace+;
#pragma link C++ class Random+;
#pragma link C++ class Resonance+;
#pragma link C++ class RelBreitWigner+;
#pragma link C++ class Flatte+;

#endif

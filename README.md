# AmplitudeModel

A project that can generate toys and evaluate amplitude models based on a resonant structure of a 3-body decay.

## Required packages
This project depends on the ROOT library. A working version must be available on your system.

## Compilation
The project has been tested and works on lxplus or on mac. To compile simply run
```
mkdir build
cd build
cmake ..
make
```
In order to work on lxplus an inital step may be needed to set up the right environment. Run
```
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-centos7-gcc8-opt
```
before compiling.

If you do not have access to the OPENMP library on your system, the package can be compiled without it by doing
```
mkdir build
cd build
cmake -DUSE_OPENMP=FALSE ,,
make
```

## Executable
There are two executables that can be run.
The first is a generic executable to create a Dalitz Plot, and its projections for a given model and is called by
```console
./build/PlotDalitz
```
The second is specific to the $D^{0} \to K^{0}_{S}K^{+}K^{-}$ analysis. It calculates the fraction of CP-even to CP-odd events in regions of phase-space. It is called by
```console
./build/CalculateFraction

```
[Documentation can be found here](https://gitlab.cern.ch/eshields/amplitudemodel/-/jobs/artifacts/documentation/file/Documentation/documents/html/index.html?job=document)
#include "AmpModel/ampcfg.h"

void AmpCfg::resonance(Resonance& res)
{
  _model->add_resonance( res.copy() );
}

void AmpCfg::resonance(Resonance* res)
{
  _model->add_resonance( res );
}

void AmpCfg::add(const char* name, Coeff& coeff, const int& resA, const int& resB,
                Parameter& mass, Parameter& width, const int l, Parameter& r)
{
  RelBreitWigner* res = new RelBreitWigner(name, coeff, resA, resB, mass, width, l, r);
  resonance( res );
  Parameter c1 = coeff.c1();
  Parameter c2 = coeff.c2();
  _params.push_back( c1.copy() );
  _params.push_back( c2.copy() );
  _params.push_back( mass.copy() );
  _params.push_back( width.copy() );
  _params.push_back( r.copy() );
}

void AmpCfg::add(const char* name, Parameter& c1, Parameter& c2, const int& resA, const int& resB,
                 Parameter& mass, Parameter& width, const int l, Parameter& r)
{
  Coeff coeff( c1 , c2 );
  RelBreitWigner* res = new RelBreitWigner(name, coeff, resA, resB, mass, width, l, r);
  resonance( res );
  _params.push_back( c1.copy() );
  _params.push_back( c2.copy() );
  _params.push_back( mass.copy() );
  _params.push_back( width.copy() );
  _params.push_back( r.copy() );
}

void AmpCfg::add(const char* name, Coeff& coeff, const int& resA, const int& resB,
                 Parameter& mass, Parameter& width, const int l, Parameter& r,
                 Parameter& gam1, Parameter& gam2, Parameter& mA2, Parameter& mB2)
{
  Flatte* res = new Flatte(name, coeff, resA, resB, mass, width, l, r, gam1, gam2, mA2, mB2);
  resonance( res );
  Parameter c1 = coeff.c1();
  Parameter c2 = coeff.c2();
  _params.push_back( c1.copy() );
  _params.push_back( c2.copy() );
  _params.push_back( mass.copy() );
  _params.push_back( width.copy() );
  _params.push_back( r.copy() );
  _params.push_back( gam1.copy() );
  _params.push_back( gam2.copy() );
  _params.push_back( mA2.copy() );
  _params.push_back( mB2.copy() );
}
 
void AmpCfg::add(const char* name, Parameter& c1, Parameter& c2, const int& resA, const int& resB,
                 Parameter& mass, Parameter& width, const int l, Parameter& r,
                 Parameter& gam1, Parameter& gam2, Parameter& mA2, Parameter& mB2)
{
  Coeff coeff( c1 , c2 );
  Flatte* res = new Flatte(name, coeff, resA, resB, mass, width, l, r, gam1, gam2, mA2, mB2);
  resonance( res );
  _params.push_back( c1.copy() );
  _params.push_back( c2.copy() );
  _params.push_back( mass.copy() );
  _params.push_back( width.copy() );
  _params.push_back( r.copy() );
  _params.push_back( gam1.copy() );
  _params.push_back( gam2.copy() );
  _params.push_back( mA2.copy() );
  _params.push_back( mB2.copy() );
}

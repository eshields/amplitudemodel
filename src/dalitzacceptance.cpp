#include "AmpModel/dalitzacceptance.h"

TFile* DalitzAcceptance::m_file = nullptr;

TH2D*  DalitzAcceptance::m_hist = nullptr;

Threaded<double> DalitzAcceptance::m_mSq12 = Threaded<double>(0.);

Threaded<double> DalitzAcceptance::m_mSq13 = Threaded<double>(0.);

double DalitzAcceptance::m_mSq12min = 0.;

double DalitzAcceptance::m_mSq12max = 0.;

double DalitzAcceptance::m_mSq13min = 0.;

double DalitzAcceptance::m_mSq13max = 0.;

std::vector<double> DalitzAcceptance::m_mSq12_list = std::vector<double>();

std::vector<double> DalitzAcceptance::m_mSq13_list = std::vector<double>();

int DalitzAcceptance::m_counter = 0;
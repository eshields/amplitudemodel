#include "AmpModel/coherentsum.h"

std::complex< double > CoherentSum::Adir(const double& t) const
{
  return ( *_DirValPtr );
}

std::complex< double > CoherentSum::Abar(const double& t) const
{
  return ( *_CnjValPrt );
}

double CoherentSum::ASqdir(const double& t) const
{
  return std::norm( Adir( t ) );
}

double CoherentSum::ASqbar(const double& t) const
{
  return std::norm( Abar( t ) );
}
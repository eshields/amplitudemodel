#include "AmpModel/secondaries.h"


Secondaries::Secondaries(const Secondaries& other)
{
  m_histF = other.m_histF;
  m_hist = other.m_hist; //Threaded<TH2D>(*((TH2D*)((other.m_hist)()->Clone())));
  m_construct_hist_vector();

  m_funcF = other.m_funcF;
  m_func = other.m_func;
}

Secondaries::Secondaries(const json& cfg)
{
  std::string hist_file = cfg["secondaries"]["file"].get<std::string>();
  std::string hist_obj  = cfg["secondaries"]["name"].get<std::string>();
  m_histF = new TFile(hist_file.c_str());
  m_histF->cd();
  m_histF->GetObject(hist_obj.c_str(),m_hist);
  m_hist->SetDirectory(0);
  m_histF->Close();
  m_construct_hist_vector();

  std::string func_file = cfg["secondaries_fraction"]["file"].get<std::string>();
  std::string func_obj  = cfg["secondaries_fraction"]["name"].get<std::string>();
  m_funcF = new TFile(func_file.c_str());
  m_funcF->cd();
  TF1* ftmp;
  m_funcF->GetObject(func_obj.c_str(),ftmp);
  const int N = ftmp->GetNpar();
  m_func.setCoefficients(N,ftmp->GetParameters());
  m_funcF->Close();
}

Secondaries::Secondaries(const std::string& hist_file, const std::string& hist_obj, const std::string& func_file, const std::string& func_obj)
{
  m_histF = new TFile(hist_file.c_str());
  m_histF->cd();
  m_histF->GetObject(hist_obj.c_str(),m_hist);
  m_hist->SetDirectory(0);
  m_histF->Close();
  m_construct_hist_vector();

  m_funcF = new TFile(func_file.c_str());
  m_funcF->cd();
  TF1* ftmp;
  m_funcF->GetObject(func_obj.c_str(),ftmp);
  const int N = ftmp->GetNpar();
  m_func.setCoefficients(N,ftmp->GetParameters());
  m_funcF->Close();
}

void Secondaries::m_construct_hist_vector()
{
  for (int i = 1; i < m_hist->GetNbinsY(); i++) {
    m_hists.push_back( m_hist->ProjectionY(std::to_string(i).c_str(),i,i+1) );
  }
}

const double Secondaries::true_time(const double& t_reco)
{
  int bin;
  #ifdef _OPENMP
    #pragma omp critical
  #endif
      bin = m_hist->GetXaxis()->FindBin( t_reco ) - 1;
  if ( bin < m_hists.size() ) {
    if ( m_hists[bin]->GetEntries() ) {
      double t_true;
      #ifdef _OPENMP
        #pragma omp critical
      #endif
          t_true = m_hists[bin]->GetRandom();
      return t_true;
    }
  }
  return t_reco;
}

const double Secondaries::fraction(const double& t_reco)
{
  return m_func.Eval( t_reco );
}

const double Secondaries::operator()(const double& t_reco)
{
  return m_func.Eval( t_reco );
}
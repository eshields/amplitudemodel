#include "AmpModel/coefficient.h"

std::complex< double > Coeff::operator*(const double& num)
{
  if ( Options::Coordinates() == "polar" ) return std::polar( _c1.val(), _c2.val() )*num;
  return std::complex<double>( _c1.val(), _c2.val() )*num;
}

std::complex< double > Coeff::operator*(const std::complex< double >& num)
{
  if ( Options::Coordinates() == "polar" ) return std::polar( _c1.val(), _c2.val() )*num;
  return std::complex<double>( _c1.val(), _c2.val() )*num;
}
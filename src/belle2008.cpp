#include "AmpModel/belle2008.h"

Belle2008::Belle2008() : belle2008(1.8645,0.49767,0.49368,0.49368)
{
  Parameter ma0_980(0.999,0.002);
  Parameter mphi(1.01955,0.00001917);
  Parameter mf0_1370(1.350,0.);
  Parameter mf2_1270(1.2754,0.);
  Parameter ma0_1450(1.474,0.019);

  Parameter wa0980(0.1135943621,0.00001);
  Parameter wphi(0.00459787,0.00003969);
  Parameter wf0_1370(0.265,0.);
  Parameter wf2_1270(0.1815,0.);
  Parameter wa_1450(0.265,0.013);

  Parameter g1a0_980(0.6219180253,0.00001);
  Parameter g2a0_980(0.7830823519,0.00001);
  Parameter mEta(0.54730,0.);
  Parameter mPic(0.139570,0.);

  Parameter rBW(1.5,0.5);

  Parameter ama00_980(1.,0.);
  Parameter ama0p_980(0.562,0.015);
  Parameter ama0m_980(0.118,0.015);
  Parameter amphi(0.227,0.005);
  Parameter amf0_1370(0.04,0.06);
  Parameter amf2_2_1270(0.261,0.020);
  Parameter ama00_1450(0.65,0.09);
  Parameter ama0p_1450(0.84,0.04);

  Parameter pha00_980(0.,0.);
  Parameter pha0p_980(to_radians(179),to_radians(3));
  Parameter pha0m_980(to_radians(138),to_radians(7));
  Parameter phphi(to_radians(-56.2),to_radians(1.0));
  Parameter phf0_1370(to_radians(-2),to_radians(80));
  Parameter phf_2_1270(to_radians(-9),to_radians(6));
  Parameter pha00_1450(to_radians(-95),to_radians(10));
  Parameter pha0p_1450(to_radians(97),to_radians(4));

  belle2008.add("a0(980)",ama00_980,pha00_980,2,3,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  belle2008.add("ap(980)",ama0p_980,pha0p_980,1,2,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  belle2008.add("am(980)",ama0m_980,pha0m_980,1,3,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  belle2008.add("phi(1020)",amphi,phphi,2,3,mphi,wphi,1,rBW);
  belle2008.add("f0(1370)",amf0_1370,phf0_1370,2,3,mf0_1370,wf0_1370,0,rBW);
  belle2008.add("f2(1270)",amf2_2_1270,phf_2_1270,2,3,mf2_1270,wf2_1270,2,rBW);
  belle2008.add("a0(1450)",ama00_1450,pha00_1450,2,3,ma0_1450,wa_1450,0,rBW);
  belle2008.add("ap(1450)",ama0p_1450,pha0p_1450,2,3,ma0_1450,wa_1450,0,rBW);

  Options::SetCoordinates("polar");
  Options::SetAngular("zemach");
  // belle2008.setNorm();
}
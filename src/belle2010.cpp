#include "AmpModel/belle2010.h"

Belle2010::Belle2010() : belle2010(1.8645,0.49767,0.49368,0.49368)
{
  Parameter ma0_980(0.999,0.002);
  Parameter mphi(1.01955,0.00001917);
  Parameter mf0_1370(1.350,0.);
  Parameter mf2_1270(1.2754,0.);
  Parameter ma0_1450(1.474,0.019);

  Parameter wa0980(0.1135943621,0.00001);
  Parameter wphi(0.00459787,0.00003969);
  Parameter wf0_1370(0.265,0.);
  Parameter wf2_1270(0.1815,0.);
  Parameter wa_1450(0.265,0.013);

  Parameter g1a0_980(0.6219180253,0.00001);
  Parameter g2a0_980(0.7830823519,0.00001);
  Parameter mEta(0.54730,0.);
  Parameter mPic(0.139570,0.);

  Parameter rBW(1.5,0.5);

  Parameter rea00_980(12.2344861,0.);
  Parameter rea0p_980(-7.56633713,0.);
  Parameter rea0m_980(-1.19249603,6.309e-03);
  Parameter rephi(-1.29497e-01,1.719e-03);
  Parameter ref0_1370(1.55304e-01,5.158e-02);
  Parameter ref2_2_1270(3.84159e-01,1.478e-02);
  Parameter rea00_1450(-2.91593e-01,1.091e-01);
  Parameter rea0p_1450(-8.72480e-02,6.182e-02);

  Parameter ima00_980(0.,0.);
  Parameter ima0p_980(-1.73609903,7.853e-03);
  Parameter ima0m_980(0.94854397,5.199e-03);
  Parameter imphi(1.91649e-01,1.403e-03);
  Parameter imf0_1370(2.37891e-02,3.476e-02);
  Parameter imf_2_1270(2.36121e-02,1.342e-02);
  Parameter ima00_1450(-7.72051e-01,8.196e-02);
  Parameter ima0p_1450(9.26874e-01,3.424e-02);
  
  belle2010.add("a0(980)",rea00_980,ima00_980,2,3,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  belle2010.add("ap(980)",rea0p_980,ima0p_980,1,2,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  belle2010.add("am(980)",rea0m_980,ima0m_980,1,3,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  belle2010.add("phi(1020)",rephi,imphi,2,3,mphi,wphi,1,rBW);
  belle2010.add("f0(1370)",ref0_1370,imf0_1370,2,3,mf0_1370,wf0_1370,0,rBW);
  belle2010.add("f2(1270)",ref2_2_1270,imf_2_1270,2,3,mf2_1270,wf2_1270,2,rBW);
  belle2010.add("a0(1450)",rea00_1450,ima00_1450,2,3,ma0_1450,wa_1450,0,rBW);
  belle2010.add("ap(1450)",rea0p_1450,ima0p_1450,2,3,ma0_1450,wa_1450,0,rBW);

  Options::SetCoordinates("rectangular");
  Options::SetAngular("zemach");
}
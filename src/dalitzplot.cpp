#include "AmpModel/dalitzplot.h"

TCanvas* DalitzPlot::plot(TH2* his)
{
  TGraph* boundary = setBoundary();
  decorate(his);

  gStyle->SetPalette( kBird );
  gStyle->SetPadRightMargin(0.14);

  TCanvas* canv = new TCanvas("canv","canv",650,600);
  canv->cd();
  gPad->SetFixedAspectRatio();

  his->Draw("COLZ");
  boundary->Draw("SAME L");

  canv->Update();

  return canv;
}

TCanvas* DalitzPlot::plotProjection(TH2* his, std::string var)
{
  decorate(his);

  gStyle->SetPadRightMargin(0.05);

  TCanvas* canv = new TCanvas("canv","canv",650,600);
  canv->cd();

  TH1* proj;
  if ( var == std::string("x") ) proj = his->ProjectionX();
  else if ( var == std::string("y") ) {
    proj = his->ProjectionY();
    proj->GetXaxis()->ImportAttributes(his->ProjectionX()->GetXaxis());
  }
  proj->GetYaxis()->SetTitle("|A|^{2}");

  proj->Draw("HIST");

  canv->Update();

  return canv;
}

TCanvas* DalitzPlot::plotProjection(std::vector<TH2*> hiss, std::string var)
{
  decorate( hiss[0] );
  gStyle->SetPadRightMargin(0.05);

  TCanvas* canv = new TCanvas("canv","canv",650,600);
  canv->cd();

  TH1* proj;
  if ( var == std::string("x") ) proj = hiss[0]->ProjectionX();
  else if ( var == std::string("y") ) {
    proj = hiss[0]->ProjectionY();
    proj->GetXaxis()->ImportAttributes(hiss[0]->ProjectionX()->GetXaxis());
  }
  proj->GetYaxis()->SetTitle("|A|^{2}");  

  proj->Draw("HIST");

  for (int i = 1; i < hiss.size(); i++) {
    if ( var == std::string("x") ) hiss[i]->ProjectionX()->Draw("SAME HIST");
    else if ( var == std::string("y") ) hiss[i]->ProjectionY()->Draw("SAME HIST");
  }

  canv->Update();

  return canv;
}

void DalitzPlot::decorate(TH2* his)
{
  his->GetXaxis()->SetTitle( (_xaxis+std::string(" [")+_unit+std::string("]")).c_str() );
  his->GetYaxis()->SetTitle( (_yaxis+std::string(" [")+_unit+std::string("]")).c_str() );
}

void DalitzPlot::setBoundaryOptions(TGraph* boundary)
{
  boundary->SetLineWidth(2);
  boundary->SetLineColor(kRed);
  boundary->SetMarkerColor(kRed);
  boundary->SetMarkerStyle(8);
  boundary->SetMarkerSize(0.4);
}

TGraph* DalitzPlot::setBoundary(const int& boundary_points)
{
  // Fill vectors with boundary points.
  std::vector< double > _mSqABbound, _mSqBCbound;
  double step = ( _ps->mSq12max() - _ps->mSq12min() ) / boundary_points;
  double mSqAB = _ps->mSq12min();
  while ( mSqAB < _ps->mSq12max() ) {
      _mSqABbound.push_back( mSqAB );
      _mSqBCbound.push_back( _ps->mSq13max( mSqAB ) );
      mSqAB += step;
  }
  mSqAB -= step;
  while ( mSqAB > _ps->mSq12min() ) {
      _mSqABbound.push_back( mSqAB );
      _mSqBCbound.push_back( _ps->mSq13min( mSqAB ) );
      mSqAB -= step;
  }
  mSqAB += step;
  _mSqABbound.push_back( mSqAB );
  _mSqBCbound.push_back( _ps->mSq13max( mSqAB ) );

  // Create boundary graph.
  TGraph* boundary = new TGraph( _mSqABbound.size(), _mSqABbound.data(), _mSqBCbound.data() );

  // Set options.
  setBoundaryOptions( boundary );

  return boundary;
}
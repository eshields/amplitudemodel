#include "AmpModel/polynomial.h"


Polynomial::Polynomial(std::initializer_list<double> coefficients) 
{
  m_coefficients.resize( m_degree );
  int i = 0;
  for (auto c : coefficients ) m_coefficients[i] = c; i++;
  m_degree = m_coefficients.size();
}

Polynomial::Polynomial(const int& size, double* coefficients) 
{
  m_coefficients.resize( size );
  for (int i = 0; i < m_degree; i++) {
    m_coefficients[i] = *coefficients;
    ++coefficients;
  }
  m_degree = m_coefficients.size();
}

void Polynomial::setCoefficients(std::initializer_list<double> coefficients)
{
  int i = 0;
  m_coefficients.resize( coefficients.size() );
  for (auto c : coefficients ) m_coefficients[i] = c; i++;
  m_degree = m_coefficients.size();
}

void Polynomial::setCoefficients(const int& size, double* coefficients)
{
  m_coefficients.resize( size );
  for (int i = 0; i < size; i++) {
    m_coefficients[i] = *coefficients;
    ++coefficients;
  }
  m_degree = m_coefficients.size();
}

double Polynomial::Eval(const double& t) const
{
  double out = 0.;
  for (int p = 0; p < m_degree; p++) {
    out += m_coefficients[p] * std::pow( t , p );
  }
  return out;
}

double Polynomial::operator()(const double& t) const
{
  double out = 0.;
  for (int p = 0; p < m_degree; p++) {
    out += m_coefficients[p] * std::pow( t , p );
  }
  return out; 
}
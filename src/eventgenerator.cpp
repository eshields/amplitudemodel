#include "AmpModel/eventgenerator.h"

EventGenerator::EventGenerator(const double& mMother, const double& m1, const double& m2, const double& m3) : 
  m_secondaries( nullptr ),
  m_use_secondaries( false ),
  m_ps( new PhaseSpace(mMother,m1,m2,m3) ),
  m_mSq12min( m_ps->mSq12min() ), 
  m_mSq12max( m_ps->mSq12max() ), 
  m_mSq13min( m_ps->mSq13min() ), 
  m_mSq13max( m_ps->mSq13max() ),
  m_mSqSum( m_ps->mSqSum() ),
  m_integer(0,1)
{
}

EventGenerator::EventGenerator(const EventGenerator& other) :
  m_secondaries( other.m_secondaries ),
  m_use_secondaries( other.m_use_secondaries ),
  m_ps( other.m_ps ),
  m_mSq12min( other.m_mSq12min ), 
  m_mSq12max( other.m_mSq12max ), 
  m_mSq13min( other.m_mSq13min ), 
  m_mSq13max( other.m_mSq13max ),
  m_mSqSum( other.m_mSqSum ),
  m_integer(0,1)
{
}

EventGenerator::EventGenerator(Secondaries* sec, const double& mMother, const double& m1, const double& m2, const double& m3) :
  m_secondaries( sec ),
  m_use_secondaries( ( sec == nullptr ) ? false : true ),
  //m_secondaries( (shist != nullptr && sfunc != nullptr) ? true : false ),
  m_ps( new PhaseSpace(mMother,m1,m2,m3) ),
  m_mSq12min( m_ps->mSq12min() ), 
  m_mSq12max( m_ps->mSq12max() ), 
  m_mSq13min( m_ps->mSq13min() ), 
  m_mSq13max( m_ps->mSq13max() ),
  m_mSqSum( m_ps->mSqSum() ),
  m_integer(0,1)
{
}

Event& EventGenerator::generate()
{
  // PID variable
  m_event["pid"]   = ( m_integer( Random::engine() ) ) ? -421 : 421;
  
  // Decay time.
  m_treco = DecayTime::time();
  if ( !m_use_secondaries ) {
    m_ttrue = m_treco;
  } else {
    if ( Random::flat() < m_secondaries->fraction( m_treco ) ) {
      m_ttrue = m_secondaries->true_time( m_treco );
      m_event["isTruePrompt"] = 0;
    } else {
      m_ttrue = m_treco;
      m_event["isTruePrompt"] = 1;
    }    
  }
  m_event["t"]     = m_treco;
  m_event["ttrue"] = m_ttrue;

  // Dalitz varaibles.
  m_event["mSq12"] = DalitzAcceptance::mSq12();
  m_event["mSq13"] = DalitzAcceptance::mSq13();
  m_event["mSq23"] = m_mSqSum - m_event["mSq12"] - m_event["mSq13"];

  return m_event;
}
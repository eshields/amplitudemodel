#include "AmpModel/model.h"

//-------------------------------------------------------------------------------------
// Amplitudes.
//-------------------------------------------------------------------------------------

void Model::calculate(const double& mSq12, const double& mSq13) 
{
  _DirVal = std::complex<double>(0.,0.);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampDir.begin(); itr != _ampDir.end(); ++itr) {
      _dirVal[itr->first] = (itr->second)->evaluate(_ps, mSq12, mSq13);
      _DirVal += _dirVal[itr->first];
  }
  _CnjVal = std::complex<double>(0.,0.);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampCnj.begin(); itr != _ampCnj.end(); ++itr) {
      _cnjVal[itr->first] = (itr->second)->evaluate(_ps, mSq12, mSq13);
      _CnjVal += _cnjVal[itr->first];
  }
}

void Model::calculate(const double& mSq12, const double& mSq13, const double& mSq23) 
{
  _DirVal = std::complex<double>(0.,0.);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampDir.begin(); itr != _ampDir.end(); ++itr) {
      _dirVal[itr->first] = (itr->second)->evaluate(_ps, mSq12, mSq13, mSq23);
      _DirVal += _dirVal[itr->first];
  }
  _CnjVal = std::complex<double>(0.,0.);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampCnj.begin(); itr != _ampCnj.end(); ++itr) {
      _cnjVal[itr->first] = (itr->second)->evaluate(_ps, mSq12, mSq13, mSq23);
      _CnjVal += _cnjVal[itr->first];
  }
}

std::complex< double > Model::dirVal(const double& mSq12, const double& mSq13)
{
  std::complex<double> A(0,0);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampDir.begin(); itr != _ampDir.end(); ++itr) {
    A += (itr->second)->evaluate(_ps, mSq12, mSq13 );
  }
  return A;
}

std::complex< double > Model::cnjVal(const double& mSq12, const double& mSq13)
{
  std::complex<double> A(0,0);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampCnj.begin(); itr != _ampCnj.end(); ++itr) {
    A += (itr->second)->evaluate(_ps, mSq12, mSq13 );
  }
  return A;
}

std::complex< double > Model::dirVal(const double& mSq12, const double& mSq13, const double& mSq23)
{
  std::complex<double> A(0,0);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampDir.begin(); itr != _ampDir.end(); ++itr) {
    A += (itr->second)->evaluate(_ps, mSq12, mSq13, mSq23);
  }
  return A;
}

std::complex< double > Model::cnjVal(const double& mSq12, const double& mSq13, const double& mSq23)
{
  std::complex<double> A(0,0);
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampCnj.begin(); itr != _ampCnj.end(); ++itr) {
    A += (itr->second)->evaluate(_ps, mSq12, mSq13, mSq23);
  }
  return A;
}

std::complex< double > Model::Adir() const
{
  return _DirVal;
}

std::complex< double > Model::Abar() const
{
  return _CnjVal;
}

std::complex< double > Model::A1() const
{
  return ( _DirVal + _CnjVal )/2.;
}

std::complex< double > Model::A2() const
{
  return ( _DirVal - _CnjVal )/2.;
}

double Model::ASqdir() const
{
  return std::norm( _DirVal );
}

double Model::ASqbar() const
{
  return std::norm( _CnjVal );
}

double Model::ASq1() const
{
  return std::norm( A1() );
}

double Model::ASq2() const
{
  return std::norm( A2() );
}

double Model::ASq() const
{
  return ASqdir() + ASqbar();
}

//-------------------------------------------------------------------------------------
// Specific resonances.
//-------------------------------------------------------------------------------------

std::complex< double > Model::Adir(const char* name) const
{
  return (_dirVal.find(name)->second);
}

std::complex< double > Model::Abar(const char* name) const
{
  return (_cnjVal.find(name)->second);
}

double Model::ASqdir(const char* name) const
{
  return std::norm( Adir(name) );
}

double Model::ASqbar(const char* name) const
{
  return std::norm( Abar(name) );
}

double Model::ASq(const char* name) const
{
  return ASqdir(name) + ASqbar(name);
}

//-------------------------------------------------------------------------------------
// Resonance helpers.
//-------------------------------------------------------------------------------------

void Model::setNorm()
{
  std::cout << "Calculating normalisations." << std::endl;
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampDir.begin(); itr != _ampDir.end(); ++itr) {
      (itr->second)->setNorm( _ps );
  }
  for (std::map<const char* , Resonance*>::const_iterator itr = _ampCnj.begin(); itr != _ampCnj.end(); ++itr) {
      (itr->second)->setNorm( _ps );
  }

}

//-------------------------------------------------------------------------------------
// Add to model.
//-------------------------------------------------------------------------------------

void Model::add_resonance(Resonance* res)
{
  Resonance* cnj = res->cnj();
  _ampDir[res->name()] = res;
  _ampCnj[cnj->name()] = cnj;
}

Model Model::operator+=(Resonance* res)
{
  Resonance* cnj = res->cnj();
  _ampDir[res->name()] = res;
  _ampCnj[cnj->name()] = cnj;
  return *this;
}

Model Model::operator=(Model model)
{
  return model;
}
#include "AmpModel/mixing.h"

std::complex< double > Mixing::Adir(const double& t, const double& mSq12, const double& mSq13) const
{
  std::complex< double > A = gp( t )*( m_model->dirVal(mSq12,mSq13) ) + (_q/_p)*gm( t )*( m_model->cnjVal(mSq12,mSq13) );
  return A;
}

std::complex< double > Mixing::Abar(const double& t, const double& mSq12, const double& mSq13) const
{
  std::complex< double > A = (_p/_q)*gm( t )*( m_model->dirVal(mSq12,mSq13) ) + gp( t )*( m_model->cnjVal(mSq12,mSq13) );
  return A;
}

std::complex< double > Mixing::Adir(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const
{
  std::complex< double > A = gp( t )*( m_model->dirVal(mSq12,mSq13,mSq23) ) + (_q/_p)*gm( t )*( m_model->cnjVal(mSq12,mSq13,mSq23) );
  return A;
}

std::complex< double > Mixing::Abar(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const
{
  std::complex< double > A = (_p/_q)*gm( t )*( m_model->dirVal(mSq12,mSq13,mSq23) ) + gp( t )*( m_model->cnjVal(mSq12,mSq13,mSq23) );
  return A;
}

double Mixing::ASqdir(const double& t, const double& mSq12, const double& mSq13) const
{
  return std::norm( Adir( t , mSq12 , mSq13 ) );
}

double Mixing::ASqbar(const double& t, const double& mSq12, const double& mSq13) const
{
  return std::norm( Abar( t , mSq12 , mSq13 ) );
}

double Mixing::ASqdir(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const
{
  return std::norm( Adir( t , mSq12 , mSq13 , mSq23 ) );
}

double Mixing::ASqbar(const double& t, const double& mSq12, const double& mSq13, const double& mSq23) const
{
  return std::norm( Abar( t , mSq12 , mSq13 , mSq23 ) );
}
#include "AmpModel/efficiency.h"

Efficiency::Efficiency(const Efficiency& other)
{
  m_corrF   = other.m_corrF;
  m_corrH   = *((TH2D*)other.m_corrH.Clone());
  m_corrS   = other.m_corrS;
}

const double Efficiency::operator()(const double& t, const double& mSq12, const double& mSq13, const double& mSq23)
{
  double eff = 1.;
  if ( m_corrS ) {
    if ( !m_corrH.GetBinContent(m_corrH.FindBin( mSq23 , t )) ) return 0;
    eff += m_corrH.Interpolate( mSq23 , t );
  }
  return eff;
}

void Efficiency::setCorrelationAcceptance(std::string& file_name, std::string& obj_name)
{
  m_corrF = new TFile(file_name.c_str());
  m_corrF->cd();
  m_corrH = *((TH2D*)m_corrF->Get(obj_name.c_str()));
  m_corrF->Close();
  m_corrS = true;
  if ( m_corrS ) std::cout << "Will introduce correlation efficiency effects" << std::endl;
}

void Efficiency::setAcceptance(json& cfg)
{
  std::string file = cfg["corr"]["file"].get<std::string>();
  std::string obj  = cfg["corr"]["name"].get<std::string>();

  m_corrF = new TFile(file.c_str());
  m_corrF->cd();
  m_corrH = *((TH2D*)m_corrF->Get(obj.c_str()));
  m_corrF->Close();
  m_corrS = true;
  if ( m_corrS ) std::cout << "Will introduce correlation efficiency effects" << std::endl;
}
#include "AmpModel/decaytype.h"

void DecayType::prepare()
{
  m_maxPdf = 652.23;
  /* 
  m_maxPdf = 0.;
  int nsteps = 1000;
  double stepx = (m_model->mSq12max() - m_model->mSq12min() )/nsteps; 
  double stepy = (m_model->mSq13max() - m_model->mSq13min() )/nsteps;
  double pdfVal;
  for (double mSq12 = m_model->mSq12min(); mSq12 < m_model->mSq12max(); mSq12 += stepx) {
    for (double mSq13 = m_model->mSq13min(); mSq13 < m_model->mSq13max(); mSq13 += stepy) {
      calculate( mSq12 , mSq13 );
      pdfVal = ASqdir( 0. , mSq12 , mSq13 , m_model->mSq23(mSq12,mSq13) );
      if ( pdfVal > m_maxPdf ) m_maxPdf = pdfVal;
    }
  }
  */
  std::cout << "maxPdf = " << m_maxPdf << std::endl;
}

bool DecayType::accept(EventGenerator* generator) 
{ 
  // Initialise 
  double pdfVal = 0.;
  int count = 10000;
  Event ev = generator->generate();
  while (count--) {
    ev = generator->generate();
    // Calculate amplitude.
    if ( ev["pid"] > 0 ) pdfVal = ASqdir( ev["ttrue"] , ev["mSq12"] , ev["mSq13"] , ev["mSq23"] );
    else                 pdfVal = ASqbar( ev["ttrue"] , ev["mSq12"] , ev["mSq13"] , ev["mSq23"] );
    pdfVal *=  m_efficiency( ev["t"] , ev["mSq12"] , ev["mSq13"] , ev["mSq23"] );
    if ( Random::flat( 0 , m_maxPdf ) < pdfVal ) return true;
  }
  return false;
}
#include "AmpModel/besIII2020.h"

BESIII2020::BESIII2020() : besIII2020(1.8645,0.49767,0.49368,0.49368)
{
  Parameter ma0_980(0.994,0.005);
  Parameter mphi(1.01955,0.00001917);
  Parameter ma0_1320(1.318,0.007);
  Parameter ma0_1450(1.474,0.019);

  Parameter wa0980(0.1135943621,0.00001);
  Parameter wphi(0.00459787,0.00003969);
  Parameter wa_1320(0.1098,0.0024);
  Parameter wa_1450(0.265,0.013);

  Parameter g1a0_980(0.6219180253,0.00001);
  Parameter g2a0_980(0.7830823519,0.00001);
  Parameter mEta(0.54730,0.);
  Parameter mPic(0.139570,0.);

  Parameter rBW(1.5,0.5);

  Parameter ama00_980(1.,0.);
  Parameter ama0p_980(0.64,0.2);
  Parameter amphi(0.74,0.14);
  Parameter ama0p_1320(0.12,0.04);
  Parameter ama0m_1320(0.09,0.05);
  Parameter ama0m_1450(0.16,0.13);

  Parameter pha00_980(0.,0.);
  Parameter pha0p_980(2.94,0.22);
  Parameter phphi(1.67,0.27);
  Parameter pha0p_1320(-2.92,0.55);
  Parameter pha0m_1320(-0.06,0.61);
  Parameter pha0m_1450(0.12,1.08);

  besIII2020.add("a0(980)",ama00_980,pha00_980,2,3,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  besIII2020.add("ap(980)",ama0p_980,pha0p_980,1,2,ma0_980,wa0980,0,rBW,g1a0_980,g2a0_980,mEta,mPic);
  besIII2020.add("phi(1020)",amphi,phphi,2,3,mphi,wphi,1,rBW);
  besIII2020.add("ap(1320)",ama0p_1320,pha0p_1320,1,2,ma0_1320,wa_1320,2,rBW);
  besIII2020.add("am(1320)",ama0m_1320,pha0p_1320,1,3,ma0_1320,wa_1320,2,rBW);
  besIII2020.add("am(1450)",ama0m_1450,pha0m_1450,1,3,ma0_1450,wa_1450,0,rBW);

  Options::SetCoordinates("polar");
  Options::SetAngular("helicity");
  //besIII2020.setNorm();
}